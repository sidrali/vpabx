package mobi.econ.vpabx

class Packages {

    String name
    int NoOfExtensions
    int packagePrice
    int pricePerExtension
    int onNetRatePerMinute
    int offNetRatePerMinute
    boolean enabled
    int validity

    static hasMany = [companyPackage: CompanyPackage]

    static constraints = {
        name(nullable: false, blank: false)
        packagePrice(nullable: false, blank: false)
        pricePerExtension(nullable: false, blank: false)
        enabled(nullable: false, blank: false, default: false)
        validity(nullable: false, blank: false, default: 0)

    }
}
