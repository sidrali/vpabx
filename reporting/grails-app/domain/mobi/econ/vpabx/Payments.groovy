package mobi.econ.vpabx

class Payments {

    int amount
    Date depositDate
    int packageID

    static belongsTo = [company:Company]

    static constraints = {
        amount(nullable: false, blank: false)
        depositDate(nullable: false, blank: false)
    }
}
