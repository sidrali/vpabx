package mobi.econ.vpabx

class VoiceBundles {

    String name
    int minutes
    int onNetMinutes
    int offNetMinutes
    int bundlePrice
    int CallRatePerMinute
    int validity
    boolean enabled

    static constraints = {
        name(nullable: false, blank: false)
        minutes(nullable: false, blank: false)
        bundlePrice(nullable: false, blank: false)
        enabled(nullable: false, blank: false, default: false)
        validity(nullable: false, blank: false, default: 0)
    }
}
