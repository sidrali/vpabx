package mobi.econ.vpabx

class Company {

    String name
    String address
    String city
    String province
    String pocName
    long pocNumber
    String pocEmailId
    int status
    String statusReason
    String registeredCNIC
    long uanId
    Date registerationDate
    int maxQueues
    Date billedDate

    static hasMany = [companyPackage: CompanyPackage, companyVoiceBundles: CompanyVoiceBundles]

    static constraints = {
        name(nullable: false, blank: false)
        pocName(nullable: false, blank: false)
        pocNumber(nullable: false, blank: false)
        status(nullable: false, blank: false)
        registeredCNIC(nullable: false, blank: false)
        uanId(nullable: false, blank: false)
    }
}
