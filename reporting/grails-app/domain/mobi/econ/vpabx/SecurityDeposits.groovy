package mobi.econ.vpabx

class SecurityDeposits {

    int amount
    Date depositDate

    static belongsTo = [company:Company]

    static constraints = {
        amount(nullable: false, blank: false)
        depositDate(nullable: false, blank: false)
    }
}
