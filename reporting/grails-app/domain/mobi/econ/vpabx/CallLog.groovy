package mobi.econ.vpabx

class CallLog implements Comparable<CallLog> {

    long callingNumber
    long calledNumber
    long answerTime
    long releaseTime
    int callDuration
    boolean incoming
    boolean intercom
    String actualDialedDigits
    boolean cdrGenerated

    static belongsTo = [company:Company]

    static constraints = {
        callingNumber(nullable: false, blank: false)
        calledNumber(nullable: false, blank: false)
        answerTime(nullable: false, blank: false)
        releaseTime(nullable: false, blank: false)
        callDuration(nullable: false, blank: false)
        incoming(nullable: false, blank: false)
        intercom(nullable: false, blank: false)
        actualDialedDigits(nullable: false, blank: false)
        cdrGenerated(nullable: false, blank: false, default: false)

    }

    static namedQueries = {
        pendingCallLogs {
            eq "cdrGenerated", false

        }
        onlyCount{
            projections {
                countDistinct('id')
            }
        }
    }

    def markCdrGenerated() {
        cdrGenerated = true
    }

    @Override
    int compareTo(CallLog o) {
        return id
    }

    String getCdrCallingNumber(){
        return "92" + callingNumber
    }

    String getCdrCalledNumber(){
        return "92" + calledNumber
    }

    int getCdrIncomingValue(){
        if(incoming) return 3 as int
        else return 2 as int
    }

    String getCdrVpnFlag(){
        if(intercom) return "f1"
        return "f2"
    }




}